################

Profile Site

################

This is a static site I made to display my profile.
Feel free to pull the code and customise it to your own liking.

Ensure to change sections with "Anirvan Bhaduri" to your own name.
Also ensure the projects/skills are your own.
Same goes for any sections you choose to have in your own version.

Anirvan
