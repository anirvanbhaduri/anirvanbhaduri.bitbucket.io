$(function() {
    $('.professional > div > .scrollable').each(function() {
        var el = $(this);
        if (el.height() > 22*12) {
            el.height(22*12); 
            el.css('overflow-y','scroll');
        }
    });

    function addCol() {
        var el = $('#id_propmole_img');
        var width = $(window).width();
        if (width < 992 && width > 767) {
            el.addClass('col');
        } else if (width < 430) {
            el.addClass('col');
        } else {
            el.removeClass('col');
        }
    }

    function adjustTicTacToe() {
        $('.square').each(function() {
            var el = $(this);
            var width = $(window).width();
            
            if (width < 415) {
                el.css('font-size','1.2em');
                el.css('line-height','2.1em');
                el.css('height','2.1em');
                el.css('width','2.1em');
            } else if (width < 768) {
                el.css('font-size','2em');
                el.css('line-height','2.5em');
                el.css('height','2.5em');
                el.css('width','2.5em');
            } else if (width < 800) {
                el.css('font-size','1.2em');
                el.css('line-height','1.8em');
                el.css('height','1.8em');
                el.css('width','1.8em');
            } else if (width < 992) {
                el.css('font-size','1.5em');
                el.css('line-height','2.1em');
                el.css('height','2.1em');
                el.css('width','2.1em');
            } else {
                el.css('font-size','2em');
                el.css('line-height','2.5em');
                el.css('height','2.5em');
                el.css('width','2.5em');
            }
        });
    }

    function reSize() {
        //addCol();
        adjustTicTacToe();
    }

    //addCol();
    adjustTicTacToe();

    window.onresize = reSize;
});
